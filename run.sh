#!/bin/bash 
current_time=$(date "+%Y%m%d%H%M%S")
rust_hello_world $1 > /output/"$2"
exit 0;

#echo "bin/bash"
# store arguments in a special array 
#args=("$@") 
# get number of elements 
#ELEMENTS=${#args[@]} 
 
# echo each element in array  
# for loop 
#for (( i=0;i<$ELEMENTS;i++)); do 
#    echo ${args[${i}]} 
#done